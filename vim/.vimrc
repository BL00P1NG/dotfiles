if empty(glob('~/.vim/autoload/plug.vim'))
	  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
	  	\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
call plug#begin('~/.vim/autoload/plugged')
	Plug 'arcticicestudio/nord-vim'
        " Plug 'chriskempson/base16-vim'
	Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
	Plug 'itchyny/lightline.vim'
	Plug 'pangloss/vim-javascript'
        Plug 'itchyny/vim-cursorword'
	Plug 'tpope/vim-surround'
	Plug 'mattn/emmet-vim'
	Plug 'skammer/vim-css-color'
	Plug 'hail2u/vim-css3-syntax'
        Plug 'sheerun/vim-polyglot'
        Plug 'elzr/vim-json'
call plug#end()

colorscheme nord
" colorscheme base16-default-dark

set laststatus=2

let g:lightline = {
    \ 'colorscheme': 'wombat',
    \ }

" NerdTree shortcut
map <C-n> :NERDTreeToggle<CR>

" Line numbers
set number

" Filetype sintax
filetype on
filetype plugin on
set omnifunc=syntaxcomplete#Complete
filetype indent on

" reload files changed outside vim
set autoread         

" encoding is utf 8
set encoding=utf-8
set fileencoding=utf-8

" by default, in insert mode backspace won't delete over line breaks, or 
" automatically-inserted indentation, let's change that
set backspace=indent,eol,start

" set unix line endings
set fileformat=unix
" when reading files try unix line endings then dos, also use unix for new
" buffers
set fileformats=unix,dos

" windows like clipboard
" yank to and paste from the clipboard without prepending "* to commands
let &clipboard = has('unnamedplus') ? 'unnamedplus' : 'unnamed'
" map c-x and c-v to work as they do in windows, only in insert mode
vm <c-x> "+x
vm <c-c> "+y
cno <c-v> <c-r>+
exe 'ino <script> <C-V>' paste#paste_cmd['i']

" save with ctrl+s
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a

" allow Tab and Shift+Tab to
" tab  selection in visual mode
vmap <Tab> >gv

vmap <S-Tab> <gv 

" search settings
"set incsearch        " find the next match as we type the search
"set hlsearch         " hilight searches by default
" use ESC to remove search higlight
"nnoremap <esc> :noh<return><esc>" search settings
"set incsearch        " find the next match as we type the search
"set hlsearch         " hilight searches by default
" use ESC to remove search higlight
"nnoremap <esc> :noh<return><esc>

" indentation
set expandtab       " use spaces instead of tabs
set autoindent      " autoindent based on line above, works most of the time
set smartindent     " smarter indent for C-like languages
set shiftwidth=4    " when reading, tabs are 4 spaces
set softtabstop=4   " in insert mode, tabs are 4 spaces

" use <C-Space> for Vim's keyword autocomplete
"  ...in the terminal
inoremap <Nul> <C-n>
"  ...and in gui mode
inoremap <C-Space> <C-n>

" On file types...
"   .md files are markdown files
autocmd BufNewFile,BufRead *.md setlocal ft=markdown
"   .twig files use html syntax
autocmd BufNewFile,BufRead *.twig setlocal ft=html
"   .less files use less syntax
autocmd BufNewFile,BufRead *.less setlocal ft=less
"   .jade files use jade syntax

autocmd BufNewFile,BufRead *.jade setlocal ft=jade


